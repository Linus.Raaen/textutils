package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestTest {
	TextAligner aligner = new TextAligner() {

		public String center(String text, int width) {
			if (text.length()>width||width<=0||width%2!=text.length()%2){
				throw new IllegalArgumentException("Ting virker ikke!!!");
			}
			int extra = (width - text.length()) / 2;
			return " ".repeat(extra) + text + " ".repeat(extra);
		}
		public void centered(String text, int width){
			aligner.center(text, width);
		}

		public String flushRight(String text, int width) {
			// TODO Auto-generated method stub
			return null;
		}

		public String flushLeft(String text, int width) {
			// TODO Auto-generated method stub
			return null;
		}

		public String justify(String text, int width) {
			// TODO Auto-generated method stub
			return null;
		}};
		
	@Test
	void test() {
		/*fail("Not yet implemented");*/
	}

	@Test
	void testCenter() {
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals(" foo ", aligner.center("foo", 5));
		assertEquals(" fo ", aligner.center("fo", 4));
		assertThrows(IllegalArgumentException.class, () -> aligner.center("foo",4));
		assertThrows(IllegalArgumentException.class, () -> aligner.center("1a",-3));
		assertThrows(IllegalArgumentException.class, () -> aligner.center("1a",3));
		assertThrows(IllegalArgumentException.class, () -> aligner.center("",0));
		

		for (int i = 1; i < 101; i++) {
			if(i%5==0&&i%3==0){
				System.out.println("fizzbuzz");
			} else if(i%5==0){
				System.out.println("buzz");
			} else if(i%3==0){
				System.out.println("fizz");
			} else{
				System.out.println("i");
			}
		}
	}




}